# Timescale Docker Image Proxy

This repo exists because we needed to workaround the Dockerhub pull rate limit for Timescale Helm chart, [see](https://github.com/timescale/helm-charts/tree/main/charts/timescaledb-single). This Chart doesn't allow to specify `imagePullSecrets`, that would have allowed us to use the proper solution: [Gitlab Dependency Proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/). Timescale Helm chart is deprecated anyway, see [new method](https://docs.timescale.com/self-hosted/latest/install/installation-kubernetes/).

We pushed the Timescale image to the container registry of this repo, which doesn't have a pull rate limit. As soon as we migrate to new method, we will get rid of this repo / registry.